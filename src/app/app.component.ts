import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  registForm: FormGroup;
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$';
  numberPattern = '^-?[0-9]\\d*(\\.\\d{1,2})?$';
  country: string[];
  months: string[];
  days: string[];
  years: string[];
  data: any[];
  //property
  submited = false;
  closeResult: string;
  constructor(private fb: FormBuilder, private modalService: NgbModal) {}
  ngOnInit(): void {
    this.createForm();
    this.country = [
      'Afghanistan',
      'Albania',
      'Algeria',
      'American Samoa',
      'Andorra',
      'Angola',
      'Anguilla',
      'Antarctica',
      'Antigua and Barbuda',
      'Argentina',
      'Armenia',
      'Aruba',
      'Australia',
      'Austria',
      'Azerbaijan',
    ];
    this.months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];
    this.days = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
    ];
    this.years = [
      '1990',
      '1991',
      '1992',
      '1993',
      '1994',
      '1995',
      '1996',
      '1997',
      '1998',
      '1999',
      '2000',
      '2001',
      '2002',
      '2003',
      '2004',
      '2005',
      '2006',
      '2007',
      '2008',
      '2009',
      '2010',
      '2011',
      '2012',
      '2013',
      '2014',
      '2015',
      '2016',
      '2017',
      '2018',
      '2019',
      '2020',
    ];
    this.data = [
      { name: 'A Friend or colleauge', value: 'A Friend or colleauge' },
      { name: 'Google', value: 'Google' },
      { name: 'Blog Post', value: 'Blog Post' },
      { name: 'News Article', value: 'News Article' },
    ];
  }
  createForm(): void {
    this.registForm = this.fb.group(
      {
        fname: ['', [Validators.required, Validators.maxLength(32)]],
        lname: ['', [Validators.required, Validators.maxLength(32)]],
        email: [
          '',
          [Validators.required, Validators.pattern(this.emailPattern)],
        ],
        area: [
          '',
          [Validators.required, Validators.pattern(this.numberPattern)],
        ],
        pnumber: [
          '',
          [Validators.required, Validators.pattern(this.numberPattern)],
        ],
        pwd: [
          '',
          [Validators.required, Validators.min(8), Validators.max(128)],
        ],
        cpwd: [
          '',
          [Validators.required, Validators.min(8), Validators.max(128)],
        ],
        str1: ['', Validators.required],
        str2: ['', Validators.required],
        city: ['', Validators.required],
        state: ['', Validators.required],
        postal: ['', Validators.required],
        country: ['', Validators.required],
        dobMonth: ['', Validators.required],
        dobDay: ['', Validators.required],
        dobYear: ['', Validators.required],
        checkArray: this.fb.array([], [Validators.required]),
        //in formGroup
        acceptTerms: [false, Validators.requiredTrue],
      },
      {
        validator: MustMatch('pwd', 'cpwd'),
      }
    );
  }
  onSubmit(data): void {
    //OnSubmit
    this.submited = true;
    if (this.registForm.invalid) {
      return;
    }
    // console.log(data);
    alert(JSON.stringify(this.registForm.value, null, 4))
  }
  // open(content): void {
  //   this.modalService
  //     .open(content, { ariaLabelledBy: 'modal-basic-title' })
  //     .result.then(
  //       (result) => {
  //         this.closeResult = `Closed with: ${result}`;
  //       },
  //       (reason) => {
  //         this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  //       }
  //     );
  // }
  // private getDismissReason(reason: any): string {
  //   if (reason === ModalDismissReasons.ESC) {
  //     return 'by pressing ESC';
  //   } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
  //     return 'by clicking on a backdrop';
  //   } else {
  //     return `with: ${reason}`;
  //   }
  // }
  onCheckboxChange(e): void {
    const checkArray: FormArray = this.registForm.get(
      'checkArray'
    ) as FormArray;

    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
    } else {
      let i = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value === e.target.value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }
}
function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}
